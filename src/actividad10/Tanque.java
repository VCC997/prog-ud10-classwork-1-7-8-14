package actividad10;

public class Tanque {

    private int cap;
    private int charge;

    Tanque(int cap){
        this.cap =cap;
    }

    public void addCharge(int cant) throws TanqueLlenoExcepcion {
        if ((this.charge + cant) > cap){
            throw new TanqueLlenoExcepcion();
        }
        this.charge +=cant;
    }

    public void quitCharge(int cant) throws TanqueVacioExcepcion {
        if ((this.charge - cant) < 0){
            throw new TanqueVacioExcepcion();
        }
        this.charge +=cant;
    }
}
