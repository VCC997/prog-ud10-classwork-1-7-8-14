package actividad10;

public class TanqueVacioExcepcion extends Exception {

    public TanqueVacioExcepcion(){
        super("Tanque vacío");
    }
}
