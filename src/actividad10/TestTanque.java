package actividad10;

public class TestTanque {

    public static void main(String[] args) {

        Tanque tanque = new Tanque(100);

        try{
            System.out.println("Agregamos 50 litros");
            tanque.addCharge(50);
            System.out.println("Agregamos 200 litros");
            tanque.addCharge(200);
            System.out.println("Quitamos 50 litros");
            tanque.quitCharge(50);
            System.out.println("Quitamos 300 litros");
            tanque.quitCharge(300);

        }catch (TanqueLlenoExcepcion | TanqueVacioExcepcion excep){
            System.out.println(excep.getMessage());
        }
    }
}
