package actividad11;

import exception.WrongMatchExcepcion;

public abstract class Animal {

    private int hambre;
    private boolean imagen;
    private String comida;
    private double tamaño;
    private String localizacion;
    private String tipo;

    private Animal couple;

    public Animal(boolean imagen, String comida, double tamaño, String localizacion, String tipo) {
        this.hambre = 0;
        this.imagen = imagen;
        this.comida = comida;
        this.tamaño = tamaño;
        this.localizacion = localizacion;
        this.tipo = tipo;
    }

    public abstract void makeNoise();

    public void eat(){
        hambre = 0;
    }

    public void aparearCon(Animal animal) throws WrongMatchExcepcion {
        if (this.getClass() != animal.getClass()){
            throw new WrongMatchExcepcion();
        }
        this.couple = animal;
    }
}
