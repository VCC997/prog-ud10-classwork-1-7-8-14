package exception;

public class WrongMatchExcepcion extends Exception {

    public WrongMatchExcepcion(){
        super("El apareamiento entre dos especies distintas es invalido");
    }
}
