package actividad11.types;

import actividad11.Animal;

public class Hipopotamo extends Animal {

    public Hipopotamo(boolean imagen, String comida, double tamaño, String localizacion, String tipo) {
        super(imagen, comida, tamaño,  localizacion, "Hipopotamo");

    }
    @Override
    public void makeNoise() {
        System.out.printf("BLUP!!!"+ "\n");
    }

}
