package actividad11.types;

import actividad11.Animal;

public class Tigre extends Animal {


    public Tigre(boolean imagen, String comida, double tamaño, String localizacion, String tipo) {
        super(imagen, comida, tamaño,  localizacion, "Tigre");
    }

    @Override
    public void makeNoise() {
        System.out.printf("MEOW!!!"+"\n");
    }
}
