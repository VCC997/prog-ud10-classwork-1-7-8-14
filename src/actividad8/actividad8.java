package actividad8;

import java.util.InputMismatchException;

public class actividad8 {

    public static void main(String[] args) {

        int[] testNumbers = {20, 10, 40, 100, 8};

        for (int numero : testNumbers ){

            try {
                getEdad(numero);
            } catch (InputMismatchException excep){
                System.out.println(excep.getMessage());
            }
        }
    }

    public static void getEdad(int numero){

        if (numero >= 10 && numero <= 50){
        System.out.println("Edad"+" "+numero+"dentro del rango");
        } else {
        throw new InputMismatchException("Edad"+" "+numero+"fuera del rango");
        }
    }
}
